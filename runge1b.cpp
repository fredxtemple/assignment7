#include <stdio.h>
#include <math.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
using namespace std;

int start=0;
fstream file3;


double f1(double x, double y1, double y2){
    return -y1*y1-y2;
}

double f2(double x, double y1, double y2){
    return 5*y1-y2;
}

double rk4(double (*f)(double,double,double), double (*m)(double,double,double), double x, double h, double *y1, double *y2) {
     
       double k1,k2,k3,k4,m1,m2,m3,m4;
       k1 = h*f(x,*y1,*y2);
       m1 = h*m(x,*y1,*y2);
       k2 = h*f(x+0.5*h,*y1+0.5*k1,*y2+0.5*m1);
       m2 = h*m(x+0.5*h,*y1+0.5*k1,*y2+0.5*m1);
       k3 = h*f(x+0.5*h,*y1+0.5*k2,*y2+0.5*m2);
       m3 = h*m(x+0.5*h,*y1+0.5*k2,*y2+0.5*m2);
       k4 = h*f(x+h,*y1+k3,*y2+m3);
       m4 = h*m(x+h,*y1+k3,*y2+m3);
       *y1 = *y1+(k1+2*k2+2*k3+k4)/6;
       *y2 = *y2+(m1+2*m2+2*m3+m4)/6; }
    
     

int main(){
   
    double xin, xfin;
    xin = 0;
    xfin = 10;
    double x=xin;
    double err=0.000001;
    double h=xfin-x; //initial stepsize
    file3.open("rungeq1new.txt",ios::out);
   
    double y1,y2;
    double y1a,y1b,y2a,y2b;
    double k1,k2,k3,k4,m1,m2,m3,m4;
    double delta1=1.0, delta2=1.0;
    double delta, h_old;
    y1 = 1.5;
    y2 = 1.5;
    file3<<"value of x "<<"value of y1 "<<"value of y2 "<<endl;
    file3<<x<<"  ,  "<<y1<<"  ,  "<<y2<<endl;
    while(x<xfin){
           h = xfin-x; //first set the step as the whole interval
           if(xfin-x>1)
           {h=1;}
           
           delta1=1.0;
           delta2=1.0;
           while((delta1 > err) || (delta2 > err)) {
           y1a = y1b = y1;
           y2a = y2b = y2;
           rk4(f1, f2, x, h, &y1a, &y2a);
           h = 0.5*h;
           rk4(f1, f2, x, h, &y1b, &y2b);
           rk4(f1, f2, x+h, h, &y1b, &y2b);
           delta1=fabs(double (y1a-y1b));
           delta2=fabs(double (y2a-y2b));
           if(delta1 >= delta1){
                      delta=delta1; }
                      else{ delta=delta2; }
               h_old=2*h;
               h = 0.9*2*h*pow(err/delta, 1/5); //safety size = 0.9 }
           
           y1 = y1b+delta1/15;
           y2 = y2b+delta2/15;
           x=x+h_old; //increment of x
           
           file3<<x<<"  ,  "<<y1<<"  ,  "<<y2<<endl;
           start++;
           
        }
 file3<<"number of points used is:"<<start<<endl;
 file3.close();
 return 0; }
 }      