#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdio>

using namespace std;
const double pi = 3.141592654;
int start=0;
fstream file4;

double erf(double x) {
	return exp(-x*x);
}
                       
double Adapt2(double (*f)(double), double l, double n, double err, double W, double fl, double fn, double fk, int i) {                 
	double k = (l + n)/2, h = n - l;                                                                  
	double p = (l + k)/2, q = (k + n)/2;                                                              
	double fp = f(p), fq = f(q);                                                                      
        start= start+2;
	double W1 = (h/12)*(fl + 4*fp + fk);                                                               
	double W2 = (h/12)*(fk + 4*fq + fn);                                                            
	double W3 = W1 + W2;                                                                       
	if (i <= 0 || fabs(W3 - W) <= 15*err) {
		if( i <=0 ) 
		cout<<"boundary reached"<<endl;
		return W3 + (W3 - W)/15; }
	return Adapt2(f, l, k, err/2, W1,  fl, fk, fq, i-1) + Adapt2(f, k, n, err/2, W2, fk, fn, fq, i-1);                     
}         

double Adapt1(double (*f)(double), double l, double n, double err, int empt) {   // number of subintervals into which [l,n] is divided    
	double k = (l + n)/2, h = n - l;                                                                  
	double fl = f(l), fn = f(n), fk = f(k);                                                           
	double W = (h/6)*(fl + 4*fk + fn); 
	start++;
	return Adapt2(f, l, n, err, W, fl, fn, fk, empt);                   
}                                                                                                   

int main(){
        file4.open("Simpsonnew.txt",ios::out);
	double l=0.0, n=2.0;
	double tolerance=1.0e-6; 
	int g=1000; 
        start=2;
	double integral = Adapt1(erf, l, n, tolerance, g); // compute integral 
	file4<<"  Integral=  "<<2*integral/sqrt(pi)<<endl;
	file4<<"  Number of point used=  "<<start<<endl;

	file4.close();

	return 0;
}